Project gigawatt_tools_database {
  database_type: 'SQL Server'
  Note: '''
    # GigawattTools - Database Project
    The diagram can be visited in the follow link: https://dbdiagram.io/d/63f2a974296d97641d822246
    
    Last Updated: 19/02/2023 22:31
  '''
}


Table auth.Groups {
  id int [pk, increment]
  name  varchar(150) [not null]
  created_at datetime [default: `GETDATE()`]
}

Table auth.GroupPermissions {
  group_id int [not null, ref: > auth.Groups.id]
  permission_id int [not null, ref: > auth.Permissions.id]

  Note: 'Abstract relationship'
}

Table auth.Permissions {
  id int [pk, increment]
  name varchar(254) [not null]
  codename varchar(100)
}

Table auth.Users {
  id int [pk, increment]
  username varchar(200) [not null]
  first_name varchar(250)
  last_name varchar(250)
  email varchar(250)
  password varchar(250) [not null]
  is_active bit [default: 1]
  last_login datetime
  is_authenticated bit
  created_at datetime [default: `GETDATE()`]
}

Table auth.UserGroups {
  id int [pk, increment]
  user_id int [not null]
  group_id int [not null]

  Note: 'Abstract relationship'
}
Ref: auth.UserGroups.user_id > auth.Users.id [delete: cascade]
Ref: auth.UserGroups.group_id > auth.Groups.id [delete: cascade]

Table auth.UserPermissions {
  id int [pk, increment]
  user_id int [ref: > auth.Users.id]
  permission_id int [ref: > auth.Permissions.id]

  Note: 'Abstract relationship'
}

Table universal.Countries {
  id int [pk, increment, not null]
  name varchar(254) [not null]
  alpha_2 varchar(2)
}

Table universal.Currencies {
  id int [pk, increment, not null]
  name varchar(250) [not null]
  plural_name varchar(250)
  code varchar(3) [not null]
  symbol varchar(5)
}

Table universal.ExpenseTypes {
  id int [pk, increment, not null]
  alpha_code varchar(10)
  description varchar(254) [not null]
  parent_id int [ref: > universal.ExpenseTypes.id]
}

Table universal.UnitsOfMeasurement as Uom {
  id int [pk, increment]
  name varchar(250) [not null]
  name_plural varchar(250)
  abbreviation varchar(5) [not null]
  numerical_value float [default: 1.00]
  base_unit int [ref: > Uom.id, note: " != [id] "]
}

Table Components {
  id int [pk, increment]
  main_id int [not null, ref: > Things.id]
  component_id int [not null, ref: > Things.id]
  quantity int [not null, default: 1]
}

Table Orders {
  id int [pk, increment]
  uuid uniqeuidentifier [default: `NEWID()`]
  external_document varchar(50)
  expense_type_id int [not null, ref: > universal.ExpenseTypes.id]
  author_id int [not null, ref: > auth.Users.id]
  amount_untaxed money
  amount_tax money
  amount_total money [not null]
  observation text
  requisition_id int [ref: > Requistions.id]
  status varchar(20) [not null, note: "canceled,done,in progress,pending"]
  terms_conditions text
  currency_id int [not null, ref: > universal.Currencies.id]
  project_id int [not null, ref: > Projects.id]
  vendor_id int [not null, ref: > Partners.id]
  created_at datetime [default: `GETDATE()`]
}

Table OrderDetails {
  id bigint [pk, increment]
  external_code varchar(20)
  demand float [not null]
  unit_price money [not null]
  value_order money
  order_id int [not null, ref: > Orders.id]
  thing_id int [not null, ref: > Things.id]
  uom_id int [not null, ref: > Uom.id]
}

Table Partners {
  id int [pk, increment]
  tax_id_number varchar(25) [not null]
  business_name varchar(254) [not null]
  tradename varchar(254)
  alias varchar(150)
  fiscal_address text
  location varchar(254)
  ubigeo char(6)
  country_id int [ref: > universal.Countries.id]
  is_active bit [not null, default: 1]
  is_bank bit
  email varchar(150)
  deactivation_date datetime
  created_at datetime [default: `GETDATE()`]
}

Table Projects {
  id int [pk, increment]
  alias varchar(150)
  name varchar(512) [not null]
  is_owned bit [default: 0]
  award_date datetime
  budget money [default: 0.00]
  customer_id int [ref: > Partners.id, not null]
  parent_id int [ref: > Projects.id]
  is_active bit [default: 1]
  created_at datetime [default: `GETDATE()`]
}


Table Requistions {
  id int [pk, increment]
  uuid uniqueidentifier [default: `NEWID()`]
  priority varchar(20) [not null, note: "low,medium,high,urgent"]
  status varchar(20) [not null, note: "canceled,done,in progress,pending"]
  observation text
  created_at datetime [default: `GETDATE()`]
  applicant_id int [not null, ref: > auth.Users.id]
  project_id int [not null, ref: > Projects.id]
}

Table RequisitionDetails {
  id bigint [pk, not null]
  demand float [default: 1.00]
  base_demand float [default: 0.00]
  is_permanent bit [default: 1]
  requested_from datetime
  requested_until datetime
  created_at datetime [default: `GETDATE()`]
  requisition_id int [not null, ref: > Requistions.id]
  thing_id int [not null, ref: > Things.id]
  uom_id int [not null, ref: > Uom.id]
}

Table Things {
  id int [pk, increment]
  code varchar(16) [not null]
  unspsc varchar(8)
  description varchar(254) [not null]
  default_uom int [ref: > Uom.id]
  kind varchar(20) [note: "storable,consumable,service"]
  picture_path varchar(260)
  blueprint_path varchar(260)
  manual_path varchar(260)
  weight float [default: 0.00]
}


TableGroup Auth {
  auth.Groups
  auth.Users
  auth.Permissions
  auth.GroupPermissions
  auth.UserPermissions
}





