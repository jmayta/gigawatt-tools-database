BULK INSERT [GigawattToolsDB_v1].[dbo].[Users]
	FROM 'E:/Data/projects/csharp/WinForms/Gigawatt/GigawattCSVDataSets/auth.Users.csv'
WITH
	(CODEPAGE = '65001'
		, FORMAT = 'CSV'
		, FIELDTERMINATOR = ','
		, ROWTERMINATOR = '\n'
		, FIRSTROW = 2
	);