BULK INSERT [GigawattToolsDB_v1].[dbo].[Countries]
	FROM 'E:/Data/projects/csharp/WinForms/Gigawatt/GigawattCSVDataSets/universal.Countries.csv'
WITH
	(CODEPAGE = '65001'
		, FORMAT = 'CSV'
		, FIELDTERMINATOR = ','
		, ROWTERMINATOR = '\n'
		, FIRSTROW = 2
	);
GO

BULK INSERT [GigawattToolsDB_v1].[dbo].[Currencies]
	FROM 'E:/Data/projects/csharp/WinForms/Gigawatt/GigawattCSVDataSets/universal.Currencies.csv'
WITH
	(CODEPAGE = '65001'
		, FORMAT = 'CSV'
		, FIELDTERMINATOR = ','
		, ROWTERMINATOR = '\n'
		, FIRSTROW = 2
	);
GO

BULK INSERT [GigawattToolsDB_v1].[dbo].[ExpenseTypes]
	FROM 'E:/Data/projects/csharp/WinForms/Gigawatt/GigawattCSVDataSets/universal.ExpenseTypes.csv'
WITH
	(CODEPAGE = '65001'
		, FORMAT = 'CSV'
		, FIELDTERMINATOR = ','
		, ROWTERMINATOR = '\n'
		, FIRSTROW = 2
	);
GO


BULK INSERT [GigawattToolsDB_v1].[dbo].[UnitsOfMeasurement]
	FROM 'E:/Data/projects/csharp/WinForms/Gigawatt/GigawattCSVDataSets/universal.universal.UnitsOfMeasurement.csv'
WITH
	(CODEPAGE = '65001'
		, FORMAT = 'CSV'
		, FIELDTERMINATOR = ','
		, ROWTERMINATOR = '\n'
		, FIRSTROW = 2
	);
GO

BULK INSERT [GigawattToolsDB_v1].[dbo].[Users]
	FROM 'E:/Data/projects/csharp/WinForms/Gigawatt/GigawattCSVDataSets/auth.Users.csv'
WITH
	(CODEPAGE = '65001'
		, FORMAT = 'CSV'
		, FIELDTERMINATOR = ','
		, ROWTERMINATOR = '\n'
		, FIRSTROW = 2
	);
GO

BULK INSERT [GigawattToolsDB_v1].[dbo].[Partners]
	FROM 'E:/Data/projects/csharp/WinForms/Gigawatt/GigawattCSVDataSets/public.Partners.csv'
WITH
	(CODEPAGE = '65001'
		, FORMAT = 'CSV'
		, FIELDTERMINATOR = ','
		, ROWTERMINATOR = '\n'
		, FIRSTROW = 2
	);
GO


BULK INSERT [GigawattToolsDB_v1].[dbo].[Projects]
	FROM 'E:/Data/projects/csharp/WinForms/Gigawatt/GigawattCSVDataSets/public.Projects.csv'
WITH
	(CODEPAGE = '65001'
		, FORMAT = 'CSV'
		, FIELDTERMINATOR = ','
		, ROWTERMINATOR = '\n'
		, FIRSTROW = 2
	);
GO

BULK INSERT [GigawattToolsDB_v1].[dbo].[Things]
	FROM 'E:/Data/projects/csharp/WinForms/Gigawatt/GigawattCSVDataSets/public.Things.csv'
WITH
	(CODEPAGE = '65001'
		, FORMAT = 'CSV'
		, FIELDTERMINATOR = ','
		, ROWTERMINATOR = '\n'
		, FIRSTROW = 2
	);
GO