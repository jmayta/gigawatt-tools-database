BULK INSERT [GigawattToolsDB_v1].[dbo].[Countries]
	FROM 'E:/Data/projects/csharp/WinForms/Gigawatt/GigawattCSVDataSets/universal.Countries.csv'
WITH
	(CODEPAGE = '65001'
		, FORMAT = 'CSV'
		, FIELDTERMINATOR = ','
		, ROWTERMINATOR = '\n'
		, FIRSTROW = 2
	);