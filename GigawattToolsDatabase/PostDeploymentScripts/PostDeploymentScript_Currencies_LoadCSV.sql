BULK INSERT [GigawattToolsDB_v1].[dbo].[Currencies]
	FROM 'E:/Data/projects/csharp/WinForms/Gigawatt/GigawattCSVDataSets/universal.Currencies.csv'
WITH
	(CODEPAGE = '65001'
		, FORMAT = 'CSV'
		, FIELDTERMINATOR = ','
		, ROWTERMINATOR = '\n'
		, FIRSTROW = 2
	);