﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Drop
       unnamed constraint on [dbo].[Orders] (Check Constraint)
       unnamed constraint on [dbo].[Things] (Check Constraint)
       unnamed constraint on [dbo].[Requisitions] (Check Constraint)
       unnamed constraint on [dbo].[Requisitions] (Check Constraint)
     Alter
       [dbo].[Projects] (Table)
     Create
       Check Constraint: unnamed constraint on [dbo].[Orders] (Check Constraint)
       Check Constraint: unnamed constraint on [dbo].[Things] (Check Constraint)
       Check Constraint: unnamed constraint on [dbo].[Requisitions] (Check Constraint)
       Check Constraint: unnamed constraint on [dbo].[Requisitions] (Check Constraint)

** Supporting actions

The compatibility level of the target schema 160 is not supported, which may result in undefined behavior. Please upgrade to a later version which supports this compatibility level.

