﻿** Highlights
     Tables that will be rebuilt
       None
     Clustered indexes that will be dropped
       None
     Clustered indexes that will be created
       None
     Possible data issues
       None

** User actions
     Create
       Foreign Key: unnamed constraint on [dbo].[Projects] (Foreign Key)
       Foreign Key: unnamed constraint on [dbo].[RequisitionDetails] (Foreign Key)
       Foreign Key: unnamed constraint on [dbo].[RequisitionDetails] (Foreign Key)
       Foreign Key: unnamed constraint on [dbo].[RequisitionDetails] (Foreign Key)
       Foreign Key: unnamed constraint on [dbo].[Requisitions] (Foreign Key)
       Foreign Key: unnamed constraint on [dbo].[Requisitions] (Foreign Key)
       Foreign Key: unnamed constraint on [dbo].[Things] (Foreign Key)
       Foreign Key: unnamed constraint on [dbo].[UnitsMeasurement] (Foreign Key)
       Foreign Key: unnamed constraint on [dbo].[UserGroups] (Foreign Key)
       Foreign Key: unnamed constraint on [dbo].[UserGroups] (Foreign Key)
       Foreign Key: unnamed constraint on [dbo].[UserPermissions] (Foreign Key)
       Foreign Key: unnamed constraint on [dbo].[UserPermissions] (Foreign Key)
       Check Constraint: unnamed constraint on [dbo].[Orders] (Check Constraint)
       Check Constraint: unnamed constraint on [dbo].[Requisitions] (Check Constraint)
       Check Constraint: unnamed constraint on [dbo].[Requisitions] (Check Constraint)
       Check Constraint: unnamed constraint on [dbo].[Things] (Check Constraint)

** Supporting actions

The compatibility level of the target schema 160 is not supported, which may result in undefined behavior. Please upgrade to a later version which supports this compatibility level.

