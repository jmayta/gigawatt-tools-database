﻿CREATE TABLE [dbo].[Projects]
(
    [id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
    [code] VARCHAR(100) NOT NULL,
    [name] VARCHAR(512) NOT NULL,
    [is_owned] BIT DEFAULT 0 NOT NULL,
    [award_date] DATETIME,
    [budget] MONEY DEFAULT 0.00,
    -- If 0.00 is limitless
    [customer_id] INT NOT NULL
        FOREIGN KEY REFERENCES [dbo].[Partners](id),
    [parent_id] INT
        FOREIGN KEY REFERENCES [dbo].[Projects](id),
    [is_active] BIT DEFAULT 1,
    [created_at] DATETIME DEFAULT GETDATE()
)
