﻿CREATE TABLE [dbo].[Orders]
(
    [id] INT PRIMARY KEY IDENTITY(1,1),
    [yearly_code] VARCHAR(6) NOT NULL,
    [external_document] VARCHAR(50),
    [expense_type_id] INT NOT NULL
        FOREIGN KEY REFERENCES [dbo].[ExpenseTypes](id),
    [author_id] INT NOT NULL
        FOREIGN KEY REFERENCES [dbo].[Users](id),
    [amount_untaxed] MONEY DEFAULT 0.00,
    [amount_tax] MONEY DEFAULT 0.00,
    [amount_total] MONEY DEFAULT 0.00 NOT NULL,
    [observation] TEXT,
    [requisition_id] INT
        FOREIGN KEY REFERENCES [dbo].[Requisitions](id)
		ON DELETE SET NULL,
    [status] VARCHAR(20) NOT NULL
        CHECK([status] IN ('pending', 'in progress', 'done', 'canceled'))
        DEFAULT 'pending',
    [terms_conditions] TEXT,
    [currency_id] INT NOT NULL
        FOREIGN KEY REFERENCES [dbo].[Currencies](id),
    [project_id] INT NOT NULL
        FOREIGN KEY REFERENCES [dbo].[Projects](id),
    [vendor_id] INT NOT NULL
        FOREIGN KEY REFERENCES [dbo].[Partners](id),
    [created_at] DATETIME DEFAULT GETDATE(),
    [registry_year] AS YEAR(created_at)
)