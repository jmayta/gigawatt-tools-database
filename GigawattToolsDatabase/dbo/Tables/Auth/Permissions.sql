﻿CREATE TABLE [dbo].[Permissions]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[name] VARCHAR(254) NOT NULL,
	[codename] VARCHAR(100)
)
