﻿CREATE TABLE [dbo].[GroupPermissions]
(
	[group_id] INT FOREIGN KEY REFERENCES [dbo].[Groups](id)
		ON DELETE CASCADE NOT NULL,
	[permission_id] INT FOREIGN KEY REFERENCES [dbo].[Permissions](id)
		ON DELETE CASCADE NOT NULL,
	CONSTRAINT UQ_group_permission UNIQUE ([group_id], [permission_id])
)
