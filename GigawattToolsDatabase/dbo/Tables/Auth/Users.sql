﻿CREATE TABLE [dbo].[Users]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[username] VARCHAR(200) UNIQUE NOT NULL,
	[first_name] VARCHAR(250),
	[last_name] VARCHAR(250),
	[email] VARCHAR(250) UNIQUE,
	[password] VARCHAR(250) NOT NULL,
	[is_active] BIT DEFAULT 1,
	[last_login] DATETIME,
	[is_authenticated] BIT,
	[created_at] DATETIME DEFAULT GETDATE()
)
