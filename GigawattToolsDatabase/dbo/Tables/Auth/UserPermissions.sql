﻿CREATE TABLE [dbo].[UserPermissions]
(
	[user_id] INT FOREIGN KEY REFERENCES [dbo].[Users](id)
			ON DELETE CASCADE,
	[permission_id] INT FOREIGN KEY REFERENCES [dbo].[Permissions](id)
			ON DELETE CASCADE,
	CONSTRAINT UQ_user_permission UNIQUE ([user_id], [permission_id])
)
