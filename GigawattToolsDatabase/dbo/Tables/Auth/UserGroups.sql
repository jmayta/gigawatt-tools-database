﻿CREATE TABLE [dbo].[UserGroups]
(
	[id] INT NOT NULL PRIMARY KEY,
	[user_id] INT FOREIGN KEY REFERENCES [dbo].[Users](id)
		ON DELETE CASCADE,
	[group_id] INT FOREIGN KEY REFERENCES [dbo].[Groups](id)
		ON DELETE CASCADE,
	CONSTRAINT UQ_user_group UNIQUE ([user_id], [group_id])
)
