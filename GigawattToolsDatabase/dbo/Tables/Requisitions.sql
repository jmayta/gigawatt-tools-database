﻿CREATE TABLE [dbo].[Requisitions]
(
    [id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
    [code] VARCHAR(100) NOT NULL UNIQUE,
    [uuid] UNIQUEIDENTIFIER DEFAULT NEWID(),
    [priority] VARCHAR(20)
        CHECK([priority] IN ('urgent', 'high', 'medium', 'low'))
        DEFAULT 'low',
    [status] VARCHAR(20)
        CHECK([status] IN ('pending', 'in progress', 'done', 'canceled'))
        DEFAULT 'pending',
    [observation] TEXT,
    [created_at] DATETIME DEFAULT GETDATE(),
    [applicant_id] INT NOT NULL
        FOREIGN KEY REFERENCES [dbo].[Users](id),
    [project_id] INT NOT NULL
        FOREIGN KEY REFERENCES [dbo].[Projects](id),
)