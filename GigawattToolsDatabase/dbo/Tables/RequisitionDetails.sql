﻿CREATE TABLE [dbo].[RequisitionDetails]
(
	[id] BIGINT PRIMARY KEY IDENTITY(1,1),
	[demand] FLOAT DEFAULT 1.00,
	[base_demand] FLOAT DEFAULT 0.00,
	[is_permanent] BIT DEFAULT 1,
	[requested_from] DATETIME,
	[requested_until] DATETIME,
	[created_at] DATETIME DEFAULT GETDATE(),
	[requisition_id] INT NOT NULL 
		FOREIGN KEY REFERENCES [dbo].[Requisitions](id),
	[thing_id] INT NOT NULL 
		FOREIGN KEY REFERENCES [dbo].[Things](id),
	[uom_id] INT 
		FOREIGN KEY REFERENCES [dbo].[UnitsMeasurement](id) NOT NULL
)