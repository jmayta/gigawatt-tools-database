﻿CREATE TABLE [dbo].[Partners]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1, 1),
	[tax_id_number] VARCHAR(25) NOT NULL UNIQUE,
	[business_name] VARCHAR(254) NOT NULL UNIQUE,
	[tradename] VARCHAR(254),
	[alias] VARCHAR(150),
	[fiscal_address] TEXT,
	[location] VARCHAR(254),
	[ubigeo] CHAR(6),
	[country_id] INT 
		FOREIGN KEY REFERENCES [dbo].[Countries](id)
		ON DELETE SET NULL NULL,
	[is_active] BIT NOT NULL,
	[is_vendor] BIT NOT NULL,
	[is_customer] BIT NOT NULL,
	[is_bank] BIT DEFAULT 0,
	[email] VARCHAR(150),
	[deactivation_date] DATETIME,
	[created_at] DATETIME DEFAULT GETDATE()
)
