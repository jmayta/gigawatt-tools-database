﻿CREATE TABLE [dbo].[Things]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[code] VARCHAR(16) NOT NULL UNIQUE,
	[unspsc] VARCHAR(8),
	[description] VARCHAR(254) NOT NULL,
	[default_uom] INT 
		FOREIGN KEY REFERENCES [dbo].[UnitsMeasurement](id)
		ON DELETE SET NULL,
	[kind] VARCHAR(20)
		CHECK([kind] IN ('storable','consumable','service')),
	[picture_path] NVARCHAR(260),
	[blueprint_path] NVARCHAR(260),
	[manual_path] NVARCHAR(260),
	[weight] FLOAT DEFAULT 0.00
)
