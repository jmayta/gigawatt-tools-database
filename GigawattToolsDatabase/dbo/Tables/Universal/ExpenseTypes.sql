﻿CREATE TABLE [dbo].[ExpenseTypes]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[alpha_code] VARCHAR(10),
	[description] VARCHAR(254) NOT NULL UNIQUE,
	[parent_id] INT FOREIGN KEY REFERENCES [dbo].[ExpenseTypes](id)
)