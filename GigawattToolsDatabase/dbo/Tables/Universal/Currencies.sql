﻿-- Divisas/Monedas
CREATE TABLE [dbo].[Currencies]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[name] VARCHAR(250) NOT NULL UNIQUE,
	[plural_name] VARCHAR(250), -- Nombre en plural (soles)
	[code] VARCHAR(3) NOT NULL UNIQUE, -- Código ISO 4217 (PEN, USD)
	[symbol] VARCHAR(5) -- Símbolo ( $ ; S/. ; € )
)
