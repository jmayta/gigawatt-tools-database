﻿CREATE TABLE [dbo].[Countries]
(
	[id] INT PRIMARY KEY IDENTITY(1,1),
	[name] VARCHAR(254) NOT NULL UNIQUE,
	[alpha_2] VARCHAR(2) UNIQUE
)
