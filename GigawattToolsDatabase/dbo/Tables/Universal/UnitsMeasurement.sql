﻿-- Unit Of Measurements
CREATE TABLE [dbo].[UnitsMeasurement]
(
    [id] INT PRIMARY KEY IDENTITY(1, 1),
    [name] VARCHAR(250) NOT NULL UNIQUE,
    [name_plural] VARCHAR(250),
    [abbreviation] VARCHAR(5) NOT NULL UNIQUE,
    [numerical_value] FLOAT DEFAULT 1.00,
    [base_unit] INT
        FOREIGN KEY REFERENCES [dbo].[UnitsMeasurement](id)
)
