﻿CREATE TABLE [dbo].[Components]
(
	[id] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[main_id] INT NOT NULL FOREIGN KEY REFERENCES [dbo].[Things](id),
	[component_id] INT NOT NULL FOREIGN KEY REFERENCES [dbo].[Things](id),
	[quantity] INT DEFAULT 1 NOT NULL,
	-- Should exist a unique combination parent+component
	CONSTRAINT UQ_Components_parent_component UNIQUE (main_id, component_id)
)