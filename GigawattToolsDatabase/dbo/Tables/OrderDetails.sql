﻿CREATE TABLE [dbo].[OrderDetails]
(
    [id] BIGINT PRIMARY KEY IDENTITY(1, 1),
    [external_code] VARCHAR(20),
    [demand] FLOAT NOT NULL DEFAULT 1.00,
    [unit_price] MONEY NOT NULL DEFAULT 0.00,
    [value_order] MONEY DEFAULT 0.00,
    [order_id] INT NOT NULL
        FOREIGN KEY REFERENCES [dbo].[Orders](id),
    [thing_id] INT NOT NULL
        FOREIGN KEY REFERENCES [dbo].[Things](id),
    [uom_id] INT NOT NULL
        FOREIGN KEY REFERENCES [dbo].[UnitsMeasurement](id)
)
