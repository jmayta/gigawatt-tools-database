CREATE TYPE [dbo].[OrderDetailType] AS TABLE (
    [external_code] VARCHAR(20) NULL,
    [demand] FLOAT NOT NULL,
    [unit_price] MONEY NULL,
    [value_order] MONEY NOT NULL,
    [thing_id] INT NOT NULL,
    [uom_id] INT NOT NULL
)
