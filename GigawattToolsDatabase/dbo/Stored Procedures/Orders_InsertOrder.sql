CREATE PROCEDURE [dbo].[usp_Orders_InsertOrder]
    -- @id is autogenerate
    -- @yearlyCode is autogenerate
    @externalDocument VARCHAR(50) = NULL,
    @expenseTypeId INT,
    @authorId INT,
    @amountUntaxed MONEY = 0.00,
    @amountTax MONEY = 0.00,
    @amountTotal MONEY,
    @observation TEXT,
    @status VARCHAR(20) = "pending",
    @termsConditions TEXT,
    @currencyId INT,
    @projectId INT,
    @vendorId INT,
    -- @createdAt is autogenerate
    -- @registryYear is autogenerate
    @orderDetail [dbo].[OrderDetailType] READONLY
AS
BEGIN
    BEGIN TRY
        DECLARE 
            @newOrderId INT = 0,
            @newYearlyCode VARCHAR(6),
            @maxSerie VARCHAR(6),
            @newSerie INT
        BEGIN TRANSACTION Register
            -- Code for generate [yearly_code]
            SELECT @maxSerie = MAX([yearly_code])
                FROM [dbo].[Orders]
            WHERE registry_year = YEAR(GETDATE());
            SET @newSerie = ISNULL(@maxSerie, 0) + 1;
            SET @newYearlyCode = RIGHT('000000' + CAST(@newSerie AS VARCHAR(6)), 6);

            -- Continue with insertion
            INSERT INTO [dbo].[Orders] 
            (
                [yearly_code],
                [external_document],
                [expense_type_id],
                [author_id],
                [amount_untaxed],
                [amount_tax],
                [amount_total],
                [observation],
                [status],
                [terms_conditions],
                [currency_id],
                [project_id],
                [vendor_id]
            )
            VALUES 
            (
                @newYearlyCode,
                @externalDocument, 
                @expenseTypeId, 
                @authorId, 
                @amountUntaxed, 
                @amountTax, 
                @amountTotal, 
                @observation, 
                @status, 
                @termsConditions,
                @currencyId,
                @projectId,
                @vendorId
            )
            
            SET @newOrderId = SCOPE_IDENTITY()

            INSERT INTO [dbo].[OrderDetails]
            (
                [external_code],
                [demand],
                [unit_price],
                [value_order],
                [order_id],
                [thing_id],
                [uom_id]
            )
            SELECT 
                [external_code],
                [demand],
                [unit_price],
                [value_order],
                @newOrderId,
                [thing_id],
                [uom_id]
            FROM @orderDetail


        COMMIT TRANSACTION Register
    END TRY
    BEGIN CATCH
        ROLLBACK TRANSACTION Register
    END CATCH
END
