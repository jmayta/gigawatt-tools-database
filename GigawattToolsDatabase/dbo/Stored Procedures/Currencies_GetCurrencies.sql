CREATE PROCEDURE [dbo].[usp_Currencies_GetCurrencies]
AS
BEGIN
	SELECT [id], [name], [plural_name], [code], [symbol] FROM [dbo].[Currencies]
END
