CREATE PROCEDURE [dbo].[usp_Things_GetThingByCode]
    (
    @code VARCHAR(16)
)
AS
SELECT
    [id],
    [code],
    [unspsc],
    [description],
    [default_uom],
    [kind],
    [picture_path],
    [blueprint_path],
    [manual_path],
    [weight]
FROM [dbo].[Things]
WHERE [code] = @code
ORDER BY [description] ASC