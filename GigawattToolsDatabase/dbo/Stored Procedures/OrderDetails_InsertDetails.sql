﻿CREATE PROCEDURE [dbo].[usp_OrderDetails_InsertDetails]
  @demand FLOAT,
  @externalCode VARCHAR(20),
  @orderId INT,
  @thingId INT,
  @unitPrice MONEY,
  @uomId INT,
  @valueOrder MONEY
AS
BEGIN
  INSERT INTO [dbo].[OrderDetails]
    (
    demand,
    external_code,
    order_id,
    thing_id,
    unit_price,
    uom_id,
    value_order
    )
  VALUES
    (
      @demand,
      @externalCode,
      @orderId,
      @thingId,
      @unitPrice,
      @uomId,
      @valueOrder
  )
END
