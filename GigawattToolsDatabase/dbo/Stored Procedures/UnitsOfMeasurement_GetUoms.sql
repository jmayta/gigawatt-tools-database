CREATE PROCEDURE [dbo].[usp_UnitsOfMeasurement_GetUoms]
AS
SELECT
    [id],
    [name],
    [name_plural],
    [abbreviation],
    [numerical_value],
    [base_unit]
FROM [dbo].[UnitsMeasurement]
ORDER BY [name] ASC