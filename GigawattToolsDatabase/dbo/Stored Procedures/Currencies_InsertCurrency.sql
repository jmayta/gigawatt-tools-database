﻿CREATE PROCEDURE [dbo].[Currencies_InsertCurrency]
	@name varchar(250),
	@plural_name varchar(250),
	@code varchar(3),
	@symbol varchar(5),
	@newId int OUTPUT
AS
BEGIN
	INSERT INTO Currencies (name, plural_name, code, symbol) VALUES (@name, @plural_name, @code, @symbol)
	SET @newId = SCOPE_IDENTITY()
END
