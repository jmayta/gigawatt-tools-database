CREATE PROCEDURE [dbo].[usp_UnitsOfMeasurement_InsertUom]
    (
    @name VARCHAR(250),
    @namePlural VARCHAR(250) = NULL,
    @abbreviation VARCHAR(5),
    @numericalValue FLOAT = 1.00,
    @baseUnit INT = NULL
)
AS
BEGIN
    INSERT INTO [dbo].[UnitsMeasurement]
    VALUES
        (
            @name,
            @namePlural,
            @abbreviation,
            @numericalValue,
            @baseUnit
    )
END