CREATE PROCEDURE [dbo].[usp_Projects_GetProjectByCode]
    @code VARCHAR(100)
AS
SELECT
    [id],
    [code],
    [name],
    [is_owned],
    [award_date],
    [budget],
    [customer_id],
    [parent_id],
    [is_active],
    [created_at]
FROM PROJECTS
WHERE [code] = @code
ORDER BY [code] ASC