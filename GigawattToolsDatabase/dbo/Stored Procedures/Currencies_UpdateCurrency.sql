﻿CREATE PROCEDURE [dbo].[usp_Currencies_UpdateCurrency]
	@id int,
	@name varchar(250),
	@plural_name varchar(250),
	@code varchar(3),
	@symbol varchar(5)
AS
BEGIN
	UPDATE Currencies
	SET [name] = @name, plural_name = @plural_name, code = @code, symbol = @symbol
		WHERE id = @id
END
