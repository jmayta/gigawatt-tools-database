CREATE PROCEDURE [dbo].[usp_Partners_GetVendorByTaxIdNumber]
    @taxIdNumber VARCHAR(25)
AS
    SELECT [id], [tax_id_number], [business_name], [trade_name], [alias], [fiscal_address], 
        [location], [ubigeo], [country_id], [is_active], [is_vendor], [is_customer], 
        [is_bank], [email], [deactivation_date], [created_at]
    FROM [dbo].[Partners]
    WHERE [tax_id_number] = @taxIdNumber

