﻿CREATE PROCEDURE [dbo].[usp_Projects_GetProjects]
AS
SELECT
    [id],
    [code],
    [name],
    [is_owned],
    [award_date],
    [budget],
    [customer_id],
    [parent_id],
    [is_active],
    [created_at]
FROM PROJECTS
ORDER BY [code] ASC