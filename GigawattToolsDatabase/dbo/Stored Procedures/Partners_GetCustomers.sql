﻿CREATE PROCEDURE [dbo].[usp_Partners_GetCustomers]
AS
SELECT 
	[id], 
	[tax_id_number], 
	[business_name], 
	[tradename], 
	[alias], 
	[fiscal_address], 
	[location], 
	[ubigeo], 
	[country_id], 
	[is_active], 
	[is_vendor],
	[is_customer],
	[is_bank],
	[email], 
	[deactivation_date], 
	[created_at] 
FROM [dbo].[Partners]
WHERE [is_customer] = 1