CREATE PROCEDURE [dbo].[usp_Projects_GetActiveProjectByCode]
    @code VARCHAR(100)
AS
SELECT
    [id],
    [code],
    [name],
    [is_owned],
    [award_date],
    [budget],
    [customer_id],
    [parent_id],
    [is_active],
    [created_at]
FROM PROJECTS
WHERE [code] = @code AND [is_active] = 1
ORDER BY [code] ASC