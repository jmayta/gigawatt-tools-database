﻿CREATE PROCEDURE [dbo].[usp_ExpenseTypes_GetExpenseTypes]
AS
SELECT
    [id],
    [alpha_code],
    [description],
    [parent_id]
FROM [dbo].[ExpenseTypes]
ORDER BY [description] ASC
