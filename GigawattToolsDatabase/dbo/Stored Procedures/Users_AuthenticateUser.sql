CREATE PROCEDURE [dbo].[usp_Users_AuthenticateUser]
  @username VARCHAR(200),
  @password VARCHAR(250)
AS
  SELECT 
    [id],
    [username],
    [first_name],
    [last_name],
    [email],
    [password],
    [is_active],
    [last_login],
    [is_authenticated],
    [created_at]
FROM [dbo].[Users]
WHERE [username] = @username AND [password] = @password
