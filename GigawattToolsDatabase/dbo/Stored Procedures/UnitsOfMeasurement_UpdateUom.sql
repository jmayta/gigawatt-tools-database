CREATE PROCEDURE [dbo].[usp_UnitsOfMeasurement_UpdateUom]
    (
    @id INT,
    @name VARCHAR(250),
    @namePlural VARCHAR(250),
    @abbreviation VARCHAR(5),
    @numericalValue FLOAT,
    @baseUnit int
)
AS
BEGIN
    UPDATE [dbo].[UnitsMeasurement]
    SET
        [name] = @name,
        [name_plural] = @namePlural,
        [abbreviation] = @abbreviation,
        [numerical_value] = @numericalValue,
        [base_unit] = @baseUnit
    WHERE [id] = @id
END
