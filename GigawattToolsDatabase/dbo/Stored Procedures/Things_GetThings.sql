﻿CREATE PROCEDURE [dbo].[usp_Things_GetThings]
AS
SELECT
    [id],
    [code],
    [unspsc],
    [description],
    [default_uom],
    [kind],
    [picture_path],
    [blueprint_path],
    [manual_path],
    [weight]
FROM [dbo].[Things]
ORDER BY [description] ASC