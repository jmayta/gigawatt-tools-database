CREATE PROCEDURE [dbo].[usp_UnitsOfMeasurement_GetUom]
    @id int
AS
SELECT
    [id],
    [name],
    [name_plural],
    [abbreviation],
    [numerical_value],
    [base_unit]
FROM [dbo].[UnitsMeasurement]
WHERE [id] = @id
